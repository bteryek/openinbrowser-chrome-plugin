chrome.tabs.onCreated.addListener(function(tab) {

	chrome.tabs.query({
		url: tab.url
	}, function(tabs) {

		if(tabs.length >= 2) {

			// we need to remove all except one

			for(i = 1; i < tabs.length; i++) {

				chrome.tabs.remove(tabs[i].id);

			}

			chrome.tabs.update(tabs[0].id, {
				url: tabs[0].url
			})

		}

		chrome.tabs.update(tabs[0].id, {
			active: true
		})

	});

});
/*
chrome.windows.onCreated.addListener(function(window) {

	console.log(window);

});*/