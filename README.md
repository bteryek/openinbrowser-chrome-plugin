# OpenInBrowser Chrome Plugin
##

Chrome Browser Plugin to go along with [OpenInBrowser](https://bitbucket.org/bteryek/openinbrowser) plugin for Sublime Text

This plugin prevents duplicate tabs from being opened, while also refreshing that tab that was to be duplicated.

[Install from the Chrome Store](https://chrome.google.com/webstore/detail/openinbrowser/ddjnoikeiiiialgjgmanaienongjhamn)